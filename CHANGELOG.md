## [2020.13.1](https://gitlab.ics.muni.cz/perun/deployment/proxyidp/check_syncrepl_extended/compare/v2020.13.0...v2020.13.1) (2023-09-08)


### Bug Fixes

* correct typo in UNKNOWN ([a0f6919](https://gitlab.ics.muni.cz/perun/deployment/proxyidp/check_syncrepl_extended/commit/a0f69191bb73339814b98691ee0ecd5bb6dacad3))

# [2020.13.0](https://gitlab.ics.muni.cz/perun/deployment/proxyidp/check_syncrepl_extended/compare/v2020.12.18...v2020.13.0) (2023-06-28)


### Bug Fixes

* flake8 violations fixed ([5b16f53](https://gitlab.ics.muni.cz/perun/deployment/proxyidp/check_syncrepl_extended/commit/5b16f53393eb3a6005fb958b939ddc351fc100eb))


### Features

* allow different credentials for provider ([1aa11fa](https://gitlab.ics.muni.cz/perun/deployment/proxyidp/check_syncrepl_extended/commit/1aa11fa22b03828bed93613cb949fe8441708422))
* setup.py, rewrited to python style, output format updated ([534248a](https://gitlab.ics.muni.cz/perun/deployment/proxyidp/check_syncrepl_extended/commit/534248af0009f9e450c6cd3ed133fe3fcfafc7aa))
* warning and critical thresholds ([a5c2e30](https://gitlab.ics.muni.cz/perun/deployment/proxyidp/check_syncrepl_extended/commit/a5c2e30f3c2fe726119781066ed221d25f779402))
